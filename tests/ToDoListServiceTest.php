<?php


namespace App\Tests;


use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Service\ToDoListService;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ToDoListServiceTest extends \PHPUnit\Framework\TestCase
{

    private User $user;
    private ToDoListService $service;
    private Item $item;
    private ToDoList $list;

    public function setUp()
    {
        parent::setUp();
        $this->user = new User("John", "Doo", 16, "foobarbaz", "test@test.fr");
        $this->service = new ToDoListService();

        $this->list = new ToDoList();
        $this->item = $this->createMock(Item::class);

        $this->list->addItem($this->item);

        $this->item->method('getToDoList')->willReturn($this->list);
        $date = new \DateTime('now');
        $date->sub(\DateInterval::createFromDateString('32 min'));
        $this->item->method('getCreation')->willReturn($date);
    }

    public function testCanAddItem()
    {
        $this->assertNotNull($this->service->canAddItem($this->item));
    }

    public function testCanAddItemOversize()
    {
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->assertNull($this->service->canAddItem($this->item));
    }

    public function testCanAddItemTimeToShort()
    {
        $this->list->addItem($this->createItem(new \DateTime('now')));
        $this->assertNull($this->service->canAddItem($this->item));
    }

    private function createItem(\DateTime $date)
    {
        $item = $this->createMock(Item::class);
        $item->method('getToDoList')->willReturn($this->list);
        $item->method('getCreation')->willReturn($date);

        return $item;
    }

}