<?php


namespace App\Tests;


use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Service\EmailService;
use PharIo\Manifest\Email;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EmailServiceTest extends WebTestCase
{

    private User $user;
    private EmailService $service;
    private \Swift_Mailer $mailer;

    public function setUp()
    {
        parent::setUp();
        $this->user = new User("John", "Doo", 19, "test@test.fr", "foobarbaz");
        $this->service = new EmailService();

        self::bootKernel();
        $this->service->container = self::$kernel->getContainer();

        $this->mailer = $this->createMock(\Swift_Mailer::class);
        $this->mailer->method('send')->willReturn(null);
    }

    public function testSendOK()
    {
        $this->assertTrue($this->service->send($this->user, $this->mailer));
    }

    public function testSendUser15()
    {
        $this->user->setAge(15);
        $this->assertFalse($this->service->send($this->user, $this->mailer));
    }

}