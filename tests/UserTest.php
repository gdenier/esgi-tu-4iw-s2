<?php

namespace App\Tests;


use App\Entity\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserTest extends TestCase
{
    private User $user;
    private MockObject $emailValidator;

    public function setUp()
    {
        parent::setUp();
        $this->emailValidator = $this->createMock(ValidatorInterface::class);
        $this->emailValidator->method('validate')
            ->willReturn([]);
        $this->user = new User("John", "Doo", 16, "test@test.fr", "foobarbaz");
    }

    public function testIsValidOK()
    {
        $this->assertTrue($this->user->isValid($this->emailValidator));
    }

    public function testIsPasswordValidBadPassword()
    {
        $this->user->setPassword("foobar");
        $this->assertFalse($this->user->isValid($this->emailValidator));
        // 60 char
        $this->user->setPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        $this->assertFalse($this->user->isValid($this->emailValidator));
        $this->user->setPassword("");
        $this->assertFalse($this->user->isValid($this->emailValidator));
    }

    public function testIsAgeValid()
    {
        $this->user->setAge(8);
        $this->assertFalse($this->user->isValid($this->emailValidator));
        $this->user->setAge(-14);
        $this->assertFalse($this->user->isValid($this->emailValidator));
        $this->user->setAge(13);
        $this->assertTrue($this->user->isValid($this->emailValidator));
    }

}