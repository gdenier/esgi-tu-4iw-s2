<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    /**
     * @Route("/", methods={"GET","HEAD"}, name="accueil")
     */
    public function getAccueil()
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->connect();
        $connected = $em->getConnection()->isConnected();
        dd($connected);
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/apropos/{subject}", methods={"GET","HEAD"}, name="about")
     */
    public function getAPropos($subject)
    {
        return $this->render('a_propos.html.twig', [
            'subject' => $subject
        ]);
    }
}