<?php


namespace App\Service;


use App\Entity\User;

class EmailService
{

    public function send(User $user, \Swift_Mailer $mailer): bool
    {
        if ($user->getAge() >= 18) {
            $message = (new \Swift_Message('New Item added to your todo list'))
                ->setFrom('noReply@example.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->container->get('twig')->render(
                    // templates/emails/registration.html.twig
                        'emails/newItem.html.twig',
                        ['user' => $user]
                    ),
                    'text/html'
                );

            $mailer->send($message);

            return true;
        }
        return false;
    }

}