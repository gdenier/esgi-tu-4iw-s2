<?php


namespace App\Service;


use App\Entity\Item;

class ToDoListService
{

    public function canAddItem(Item $item): ?Item
    {
        if (count($item->getToDoList()->getItems()) > 9) {
            return null;
        }
        $now = new \DateTime('now');
        foreach ($item->getToDoList()->getItems() as $tmpItem) {
            if ($now->getTimestamp() - $tmpItem->getCreation()->getTimestamp() < 1800) {
                return null;
            }
        }
        return $item;
    }

}